# websearcher

#### 介绍
本项目是 51cto-爬虫检索轻应用-同步代码

#### 主要代码说明

- NetFetcher 简易的网页抓取Demo
- DyttDetailsFetcher 电影详情页抓取程序
- DyttLinksFetcher 电影列表页抓取程序
- DyttTagsFetcher 电影数据标签抽取程序
- EsFilmImporterTest 电影数据导入ES的测试类
- SearchBoot Web程序主要入口



#### 使用说明

**打包命令**

mvn package -Dmaven.test.skip=true

#### 相关问题

在本项目学习中如有任何问题，可关注"美码师"公众号进行留言反馈

![](https://images.cnblogs.com/cnblogs_com/littleatp/1241412/o_qrcode_for_gh_b2cf486409a0_258.jpg)


[美码师博客园地址](https://www.cnblogs.com/littleatp/)

