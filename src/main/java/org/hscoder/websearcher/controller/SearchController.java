package org.hscoder.websearcher.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SearchController {

    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World! ";
    }
}
