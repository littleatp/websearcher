package org.hscoder.websearcher.controller;

import org.hscoder.websearcher.domain.EsFilm;
import org.hscoder.websearcher.domain.EsTags;
import org.hscoder.websearcher.service.EsFilmService;
import org.hscoder.websearcher.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/film")
public class FilmController {

    @Autowired
    private EsFilmService filmService;

    /**
     * 检索页面
     * @return
     */
    @GetMapping
    public String index(){

        return "film";
    }

    /**
     * 条件标签接口
     * @return
     */
    @GetMapping("/tags")
    @ResponseBody
    public EsTags tags(){

        return filmService.getTags();
    }

    /**
     * 电影检索接口
     * @param period
     * @param language
     * @param sort
     * @param region
     * @param page
     * @param size
     * @return
     */
    @PostMapping("/films")
    @ResponseBody
    public PageResult<EsFilm> films(@RequestParam(value = "period", required = false) String period,
                                    @RequestParam(value = "language", required = false) String language,
                                    @RequestParam(value = "sort", required = false) String sort,
                                    @RequestParam(value = "region", required = false) String region,
                                    @RequestParam(value = "page", required = false) int page,
                                    @RequestParam(value = "size", required = false) int size){


        if(page <= 0){
            page =  1;
        }

        if(size <= 0){
            size = 20;
        }
        Page<EsFilm> films = filmService.search(period,sort,region,language,page,size);
        return PageResult.of(films.getTotalElements(), films.getContent());
    }
}
