package org.hscoder.websearcher.domain;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 电影信息
 */
public class FilmData {
    //来源URL
    private String url;
    //片名
    private String name;
    //原名
    private String localName;
    //地区
    private List<String> regions;
    //年代
    private String period;
    //语言
    private String language;
    //片长
    private String during;
    //评分(豆瓣)
    private double score;
    //上映日期
    private Date releaseDate;
    //分类
    private List<String> sorts;
    //标签
    private List<String> tags;
    //导演
    private String director;
    //编剧
    private String scriptwriter;
    //演员
    private List<String> actors;
    //简介
    private String intro;

    //封面照
    private String coverImgUrl;
    //剧照
    private List<String> captureImgUrls;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public List<String> getRegions() {
        return regions;
    }

    public void setRegions(List<String> regions) {
        this.regions = regions;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getDuring() {
        return during;
    }

    public void setDuring(String during) {
        this.during = during;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<String> getSorts() {
        return sorts;
    }

    public void setSorts(List<String> sorts) {
        this.sorts = sorts;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getScriptwriter() {
        return scriptwriter;
    }

    public void setScriptwriter(String scriptwriter) {
        this.scriptwriter = scriptwriter;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCoverImgUrl() {
        return coverImgUrl;
    }

    public void setCoverImgUrl(String coverImgUrl) {
        this.coverImgUrl = coverImgUrl;
    }

    public List<String> getCaptureImgUrls() {
        return captureImgUrls;
    }

    public void setCaptureImgUrls(List<String> captureImgUrls) {
        this.captureImgUrls = captureImgUrls;
    }

    public void addCaptureImgUrl(String captureImgUrl) {
        if (StringUtils.isEmpty(captureImgUrl)) {
            return;
        }
        if (this.captureImgUrls == null) {
            this.captureImgUrls = new ArrayList<>();
        }
        if (!this.captureImgUrls.contains(captureImgUrl)) {
            this.captureImgUrls.add(captureImgUrl);
        }
    }

    public void addActor(String actor) {
        if (StringUtils.isEmpty(actor)) {
            return;
        }
        if (this.actors == null) {
            this.actors = new ArrayList<>();
        }
        if (!this.actors.contains(actor)) {
            this.actors.add(actor);
        }
    }
}
