package org.hscoder.websearcher.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * 存储于ES 的检索条件
 */
@Document(type="tags", indexName = "dytttags")
public class EsTags {

    public static final String GLOBAL_ID = "global";

    @Id
    private String id = GLOBAL_ID;

    private List<TagScore> regions;
    private List<TagScore> periods;
    private List<TagScore> languages;
    private List<TagScore> sorts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<TagScore> getRegions() {
        return regions;
    }

    public void setRegions(List<TagScore> regions) {
        this.regions = regions;
    }

    public List<TagScore> getPeriods() {
        return periods;
    }

    public void setPeriods(List<TagScore> periods) {
        this.periods = periods;
    }

    public List<TagScore> getLanguages() {
        return languages;
    }

    public void setLanguages(List<TagScore> languages) {
        this.languages = languages;
    }

    public List<TagScore> getSorts() {
        return sorts;
    }

    public void setSorts(List<TagScore> sorts) {
        this.sorts = sorts;
    }
}
