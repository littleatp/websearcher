package org.hscoder.websearcher.domain;

import java.util.List;

/**
 * 带分值的标签
 */
public class TagScore implements Comparable<TagScore> {
    private String tag;
    private int score;

    public TagScore(){}

    public TagScore(String tag, int score) {
        this.tag = tag;
        this.score = score;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public int compareTo(TagScore o) {
        return o.score - this.score;
    }
}


