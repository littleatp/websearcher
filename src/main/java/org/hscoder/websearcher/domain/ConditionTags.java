package org.hscoder.websearcher.domain;

import java.util.List;

/**
 * 条件标签(用于检索)
 */
public class ConditionTags {

    private List<TagScore> regions;
    private List<TagScore> periods;
    private List<TagScore> languages;
    private List<TagScore> sorts;

    public List<TagScore> getRegions() {
        return regions;
    }

    public void setRegions(List<TagScore> regions) {
        this.regions = regions;
    }

    public List<TagScore> getPeriods() {
        return periods;
    }

    public void setPeriods(List<TagScore> periods) {
        this.periods = periods;
    }

    public List<TagScore> getLanguages() {
        return languages;
    }

    public void setLanguages(List<TagScore> languages) {
        this.languages = languages;
    }

    public List<TagScore> getSorts() {
        return sorts;
    }

    public void setSorts(List<TagScore> sorts) {
        this.sorts = sorts;
    }
}
