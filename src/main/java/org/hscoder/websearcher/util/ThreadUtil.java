package org.hscoder.websearcher.util;

import java.util.Scanner;

/**
 * 线程测试辅助工具
 * 
 * @author tangzz
 * @createDate 2017年1月30日
 * 
 */
public class ThreadUtil {

    public static void waitSome() {
        waitSome(3000);
    }

    public static void waitSome(long millSeconds) {
        try {
            Thread.sleep(millSeconds);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 等待输入指令
     * 
     * @param word
     */
    public static void waitInput(String word) {
        Scanner scanner = new Scanner(System.in);
        try {
            while (true) {
                String line = scanner.nextLine();
                if (line.trim().equalsIgnoreCase(word)) {
                    break;
                }
            }
        } finally {
            scanner.close();
        }

    }
}
