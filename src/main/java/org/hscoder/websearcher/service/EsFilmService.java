package org.hscoder.websearcher.service;

import org.hscoder.websearcher.domain.EsFilm;
import org.hscoder.websearcher.domain.EsTags;
import org.hscoder.websearcher.domain.TagScore;
import org.hscoder.websearcher.repository.EsFilmRepository;
import org.hscoder.websearcher.repository.EsTagsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.awt.print.Book;
import java.util.List;

/**
 * 电影数据检索服务
 */
@Service
public class EsFilmService {

    private static final Logger logger = LoggerFactory.getLogger(EsFilmService.class);

    @Autowired
    private EsFilmRepository filmRepository;
    @Autowired
    private EsTagsRepository tagsRepository;

    /**
     * 获取标签数据
     * @return
     */
    public EsTags getTags(){
        return tagsRepository.findById(EsTags.GLOBAL_ID).orElse(null);
    }

    /**
     * 根据条件检索数据
     * @param period
     * @param sort
     * @param region
     * @param language
     * @param page
     * @param size
     * @return
     */
    public Page<EsFilm> search(String period, String sort, String region, String language, int page, int size) {
        PageRequest pageable = PageRequest.of(page-1, size);
        return filmRepository.interSearch( period,  sort,  region,  language, pageable);
    }
}
