package org.hscoder.websearcher.repository;

import org.hscoder.websearcher.domain.EsFilm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

public interface EsFilmRepositoryCustom {

    Page<EsFilm> interSearch(String period, String sort, String region, String language, Pageable pageable);
}
