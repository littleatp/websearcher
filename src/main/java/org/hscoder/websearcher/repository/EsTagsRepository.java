package org.hscoder.websearcher.repository;

import org.hscoder.websearcher.domain.EsFilm;
import org.hscoder.websearcher.domain.EsTags;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EsTagsRepository extends ElasticsearchRepository<EsTags, String> {

}
