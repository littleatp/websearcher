package org.hscoder.websearcher.repository;

import org.hscoder.websearcher.domain.EsFilm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

public interface EsFilmRepository extends ElasticsearchRepository<EsFilm, String>, EsFilmRepositoryCustom {

    Page<EsFilm> findByPeriod(String period, Pageable pageable);
    Page<EsFilm> findByRegions(String region, Pageable pageable);

}
