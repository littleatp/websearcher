
//条件对象
var conditions = {
  sorts: [],
  regions: [],
  periods: [],
  languages: []
};
//电影列表对象
var filmList = [];

window.vm = new Vue({
  el: '#app',
  data: {
    //检索条件
    conditions: conditions,
    //选择的条件
    selected: {},
    //电影列表数据
    filmList: filmList,
    //加载状态
    isloading: false,
    //当前电影信息(对话框)
    current: null,
    pager:{
      page: 1,
      size: 20,
      pageCount: 0,
      totalCount: 0,
    }
  },
  computed: {
    selectConditionString: function(){
      var dl = [];
      for(var i in this.selected){
        dl.push( i + ":" + this.selected[i].tag)
      }
      return dl.join(',');
    },
    pagerGroup: function(){
        var current = this.pager.page;
        var pageCount = this.pager.pageCount;

        var groupSize = 8;
        var pageList = [ current ];

        var left = current -1;
        var right = current +1;

        while (pageList.length < groupSize) {

            if(left > 0){
                pageList.push(left--);
            }

            if (pageList.length < groupSize && right <= pageCount) {
                pageList.push(right++);
            }

            if(left <= 0 && right > pageCount) {
                break;
            }
        }

        //页码排序
        pageList.sort(function(a,b){ return a-b });
        return pageList;
    }
  },
  filters: {
    //评分格式(保留小数点后1位)
    scoreFormat: function (value) {
      if (!value) return "0.0";
      return Number(value).toFixed(1);
    },
    concat: function (value) {
      if (!value) return "None";
      return value.join("/")
    }
  },
  methods: {

    //动态计算选中样式
    selectedClass: function(group, value){
        if(this.selected[group] == value){
          return "btn-danger";
        }else{
          return "btn-light";
        }
    },
    //切换条件
    switchSelected: function(group, value){
        if(this.selected[group] != value){
            //重置页码
            this.pager.page = 1;
            //使用$set 操作实现动态更新(否则所有的属性就必须在data域中事先声明)
            this.$set(this.$data.selected, group, value);
        }
    },
    //切换页码
    switchPage: function(page){
        this.pager.page = page;
        this.loadData();
    },
    //固定条件展示(最多展示7个，并添加"全部"选项
    fixCondition: function(items){
        var rItems = [];
        rItems.push({tag: "全部", isall: true});

        var max = 10;
        if(items){
          rItems = rItems.concat(items.slice(0, max));
        }
        return rItems;
    },
    loadData: function(){
      this.isloading = true;
      var that = this;

      var params = {
          period: this.selected.periods.isall? '': this.selected.periods.tag,
          language: this.selected.languages.isall? '': this.selected.languages.tag,
          sort: this.selected.sorts.isall? '': this.selected.sorts.tag,
          region: this.selected.regions.isall? '': this.selected.regions.tag,
          page: this.pager.page,
          size: this.pager.size
      };
      this.$http.post('/film/films',params,{emulateJSON:true}).then(function(res){

           that.isloading = false;
           console.log(res.body);

           //电影列表
           this.filmList = res.body.list;

           //条目数量
           this.pager.totalCount = res.body.totalCount;
           this.pager.pageCount = parseInt(Number((this.pager.totalCount - 1)/this.pager.size).toFixed(0)) + 1;

         },function(res){
           console.log("error" + res.status);
       });

    },

    //展示详情
    showInfo: function(index){
        var film = this.filmList[index];
        this.current = film;
        $('#myModal').modal('show')
    }

  },

  //创建对象时，初始化条件
  created: function() {
      this.$http.get('/film/tags',{},{emulateJSON:true}).then(function(res){
            //console.log(res.body);
            var estags = res.body;
            estags.languages = this.fixCondition(estags.languages);
            estags.sorts = this.fixCondition(estags.sorts);
            estags.regions = this.fixCondition(estags.regions);
            estags.periods = this.fixCondition(estags.periods);

            this.conditions = estags;
            this.selected = {
                languages: estags.languages[0],
                sorts: estags.sorts[0],
                regions: estags.regions[0],
                periods: estags.periods[0],
            }

            console.log(this.conditions)

          },function(res){
            console.log("error" + res.status);
          });
  }
});

vm.$watch("selected", function(newVal, oldVal){
   this.loadData();
}, {deep: true});

//vm.$watch("pager.page", function(newVal, oldVal){
//   this.loadData();
//}, {deep: true});

