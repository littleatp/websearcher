package org.hscoder.filmsearcher;

import org.hscoder.websearcher.domain.EsTags;
import org.hscoder.websearcher.repository.EsTagsRepository;
import org.hscoder.websearcher.service.EsFilmImporter;
import org.hscoder.websearcher.SearchBoot;
import org.hscoder.websearcher.domain.EsFilm;
import org.hscoder.websearcher.repository.EsFilmRepository;
import org.hscoder.websearcher.service.EsFilmService;
import org.hscoder.websearcher.util.JsonUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchBoot.class)
public class EsFilmImporterTest {

    @Autowired
    private EsFilmRepository filmRepository;
    @Autowired
    private EsTagsRepository tagsRepository;

    @Autowired
    private EsFilmImporter importer;

    @Before
    public void initData() {
        //清空数据
        tagsRepository.deleteAll();
        filmRepository.deleteAll();

        //导入检索条件
        importer.importTags("D:/temp/dytt/dytt.tags");
        //导入电影信息
        importer.importFilms("D:/temp/dytt/detail");
    }

    @After
    public void disposeData() {
    }

    @Test
    public void testSome() {

        //查询检索条件信息
        EsTags tags = tagsRepository.findById(EsTags.GLOBAL_ID).get();

        System.out.println("tags.periods-- " + tags.getPeriods().size());
        System.out.println("tags.regions-- " + tags.getRegions().size());
        System.out.println("tags.sorts-- " + tags.getSorts().size());
        System.out.println("tags.languages-- " + tags.getLanguages().size());


        //查询2019的电影信息
        List<EsFilm> films = filmRepository.findByPeriod("2019", PageRequest.of(1, 20)).getContent();
        System.out.println("2019 -- " + films.size());

        //查询中国的电影信息
        films = filmRepository.findByRegions("中国", PageRequest.of(1, 20)).getContent();
        System.out.println("中国 -- " + films.size());
    }
}
